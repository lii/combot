Combot
======

Combot is a system for generating GUI:s for command line program. It is developed to make command line utilities more usable for users who are only familiar with graphical point-and-click interfaces.

Accessing and and building the source code
------------------------------------------

To download the source code of the project, run the command:
`git clone --recursive <repository name>`

The `--recursive` flag ensures that also sub repositories on which this project depends are cloned.

To build, import all Eclipse projects in the repositaty root into Eclipse. 

Run the `j.combot.app.Main` class.

The application might leave some preference data on disk. Unix systems the `~/.java/.userPrefs/j/combot` directory is used.


Used tools and technologies 
---------------------------

* The GUI is developed using the Standard Widget Toolkit (SWT).
* It uses the Java process API to start command line programs.
* It prints log messages using the Java logging API.
* It saves preferences using the Java performance API.
* It relies on multi-threading to run commands and updating the GUI simultaneously.
* It uses reflection to dynamically discover and load command factory classes. 

Command example
---------------

Combot takes command specifications in the form of Java classes. It uses the specifications to generate dialogs for giving the input that commands need, running them and presenting their output to the user.

An example of a generated dialog can be seen in the image below.

![Combot generated GUI](https://bitbucket.org/lii/combot/raw/master/Find_command.png)

This dialog is an interface for the most basic feature of the Unix `find` command. The location parameter is a file path and this creates a field in the dialog with a button that opens a file chooser dialog. There are also text arguments, number arguments and optional arguments.

The command is run by pressing the `Start` button. The output produced when running command is captured and showed is the list box bellow the button. In this example it shows all files in the Combot project directory which have the string `Arg` in their names.

The following image shows how Combot can give useful error messages, for example if the user gives a search path to a non-existing file.

![Combot generated GUI showing errors](https://bitbucket.org/lii/combot/raw/master/Find_errors.png)

### Command specification

Commands are specified by creating a `CommandFactory` class and make it available to Combot. The following code shows the specification of the `find` command that is showed above.

The factory creates a `Command` object which takes argument objects as parameters to its constructor. The argument objects themselves take sub arguments as parameters. In this way a hierarchical GUI structure can be created.
 
    public class find implements CommandFactory {
        public Command make() {
            return new Command( "Find command", "find",
                        new FileArg( "Location", "", "/", FileOrDir.DIR, FileVal.ERROR_NOT_EXIST ),
    
                        new StringArg( "Name", "-name", "*" ),
    
                        new OptArg( false,
                            new CompositeArg( "Limit depth",
                                new IntArg( "Search depth", "-maxdepth", 0, Integer.MAX_VALUE, 10 ) ) ),
                        new OptArg( false, new ExtraArg( "Extra arguments" ) ),
    
                        new AltArg( "Symbolic link treatment", 1, 10,
                                new ConstArg( "Never follow links", "-P" ),
                                new ConstArg( "Follow links", "-L" ),
                                new ConstArg( "Only follow links in args", "-H" ) )
            );
        }
    }



Credits
-------

Developed by Jens Lidestrom.



 